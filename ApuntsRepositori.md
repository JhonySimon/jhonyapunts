# Documentation
## Git key/config

```
Key generator ssh:
cat ~/.ssh/id_rsa.pub
ssh ro-(clau publica)

Git clonation (welcome):
git clone git@gitlab.com:isard/welcome.git

Git configuration user/email:
git config --global user.email "jhony@isardvdi.com"
git config --global user.name "jhony.simon"
```

## Git commands 
```
git pull
git log
git remote -v
git key
git rebase -i
git reset --hard origin/main
git status
git stash
git stash apply
git fetch (--all)
```
## Git branch
```
git branch
git branch -D (example-branch)
git checkout -b (example-branch)
git push --set-upstream origin (example-branch)
```

## Git upload
```
git add (file)
git commmit -am 'commit'
git push 
```
## Git upload without password
```
git config --global credential.helper store
```

## Install docker

```
sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

```
## Docker engine
```
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker (user)
```
## Key generator (docker)

``` 
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

```

## Docker commands
```
systemctl status docker
docker ps (--all)

docker image ls
docker volume ls
docker network ls

docker pull
docker run -d -p 8080:80 (example-nextcloud)
docker start (example_name)
docker stop (example_name)
docker rm (-f force)(example_name)
```

## Docker Examples (Nextcloud)
```
docker run -d -p 8080:80 --name minxt nextcloud
docker run -d -p 80:80 -v /opt/nextcloud/html:/var/www/html -v /opt/nextcloud/db:/var/lib/mysql --name minxt nextcloud
``` 

## Container orchestration
``` 
docker-compose
docker swarm
kubernetes
```

## Install python pip/docker-compose
```
apt install python3-pip
pip3 install docker-compose
docker-compose
```

## Docker-compose upload
```
docker-compose up -d (pull image)
docker image ls
docker ps (--all)
``` 

## Docker-compose commands
```
docker-compose down (stop)
docker-compose -f example.yml up -d
docker rmi (IdImage)
docker logs (example_db_1)
docker system prune (--all)
```

