# Grafana documentation
## Connect: Grafana + InfluxDB2 


Error:

    If you want to add an InfluxDB 2.x datasource with InfluxQL as a Grafana datasource: InfluxDB Error: Bad Request

Cause:

    The authentication "Basic auth" in the InfluxDB datasource has an error/bug. 

Solution:

    - Uncheck "Basic auth" in the Grafana InfluxDB datasource

    - Add Custom HTTP Headers in the following format (Important: the Value-field must contain "Token" AND your Token):
        - Header: Authorization
        - Value: Token <YOUR_TOKEN> --> (env.influxdb2)



![Alt text](http://wiki.webperfect.ch/images/5/51/01_influxdb_v2_header.png "InfluxDB")

## Connect: Grafana + Loki (Logs)

1. Log into your Grafana instance. If this is your first time running Grafana, the username and password are both defaulted to admin.

2. In Grafana, go to Configuration > Data Sources via the cog icon on the left sidebar.

3. Click the big + Add data source button.

4. Choose Loki from the list.

5. The http URL field should be the address of your Loki server. For example, when running locally or with Docker using port mapping, the address is likely http://localhost:3100. When running with docker-compose or Kubernetes, the address is likely http://loki:3100.

6. To see the logs, click Explore on the sidebar, select the Loki datasource in the top-left dropdown, and then choose a log stream using the Log labels button.

![Alt text](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.r15cookie.com%2Fpost%2F2021-01-02-loki%2Fimage5.png&f=1&nofb=1 "Loki")

## Connect: Grafana Alerts + TelegramBot

### Add a contact point
<br>

1. In the Grafana menu, click the Alerting (bell) icon to open the Alerting page listing existing alerts.
    
2. Click Contact points to open the page listing existing contact points.
    
3. Click New contact point.
    
4. From the Alertmanager dropdown, select an Alertmanager. By default, Grafana Alertmanager is selected.

5. In Name, enter a descriptive name for the contact point.
    
6. From Contact point type, select a type and fill out mandatory fields. For example, if you choose email, enter the email addresses. Or if you choose Slack, enter the Slack channel(s) and users who should be contacted.

7. Some contact point types, like email or webhook, have optional settings. In Optional settings, specify additional settings for the selected contact point type.
    
8. In Notification settings, optionally select Disable resolved message if you do not want to be notified when an alert resolves.
    
9. To add another contact point type, click New contact point type and repeat steps 6 through 8.
    
10. Click Save contact point to save your changes.
<br>

### Create Telegram Contact Point
<br>

You will need a BOT API Token and Chat ID.

1. Open Telegram, and create a new Bot by searching for @BotFather and then typing /newbot

2. Follow the prompts to create a name and username for your bot. The username must end in bot and must be unique.

3. You will get given a HTTP API token which is your BOT API Token to be used in Grafana. It will be in the form XXXXXXXXX: YYYYYYYYYYYYYYYYYYYYYYYYYYYYY

4. You then need the Chat ID. To get this, you first need to create a group and then add the new bot to it.

5. Then press the View Group Info option for the group, click your bot users name in the members list, and press the SEND MESSAGE button.

6. Then send any message to the user.

7. Then, in your browser, visit the url https://api.telegram.org/botXXX:YYY/getUpdates (replace the XXX:YYY with the BOT API token you just got from Telegram)

    In the JSON response, you should see a node with a message that has the type=group. This node will also have an Id. Copy this Id into the Chat ID field in Grafana.

8. Now you can test the new Telegram contact point in Grafana using the Test button.

    There should be a sample alert from Grafana inside your new Telegram group messages window.
