# Loki:

Unlike other logging systems, Loki is built around the idea of only indexing metadata about your logs: labels (just like Prometheus labels). Log data itself is then compressed and stored in chunks in object stores such as S3 or GCS, or even locally on the filesystem. A small index and highly compressed chunks simplifies the operation and significantly lowers the cost of Loki. 

A Loki-based logging stack consists of 3 components:

- Promtail is the agent, responsible for gathering logs and sending them to Loki.
- Loki is the main server, responsible for storing logs and processing queries.
- Grafana for querying and displaying the logs.

## Grafana Loki Storage
Grafana Loki needs to store two different types of data: chunks and indexes.

Loki receives logs in separate streams, where each stream is uniquely identified by its tenant ID and its set of labels. As log entries from a stream arrive, they are compressed as “chunks” and saved in the chunks store. See chunk format for how chunks are stored internally.

The index stores each stream’s label set and links them to the individual chunks.

Refer to Loki’s configuration for details on how to configure the storage and the index.

## Grafana Loki Storage Retention:

Retention in Grafana Loki is achieved either through the Table Manager or the Compactor.

Retention through the Table Manager is achieved by relying on the object store TTL feature, and will work for both boltdb-shipper store and chunk/index store. However retention through the Compactor is supported only with the boltdb-shipper store.

The Compactor retention will become the default and have long term support. It supports more granular retention policies on per tenant and per stream use cases.

<blockquote>
<p> It seems Grafana limits the amount of logs returned in a query to 1000 lines. I limited my search to a single host and a specific log message and I now get results that are hours old.

For the record, the line limit can be adjusted in the Loki datasource config. Increasing it may cause a browser performance hit though.
</p>
</blockquote>

## Retention Configuration
<blockquote>
<p>Retention is only available if the index period is 24h.
</p></blockquote>

This compactor configuration example activates retention:

```
compactor:
  working_directory: /data/retention
  shared_store: gcs
  compaction_interval: 10m
  retention_enabled: true
  retention_delete_delay: 2h
  retention_delete_worker_count: 150
schema_config:
    configs:
      - from: "2020-07-31"
        index:
            period: 24h
            prefix: loki_index_
        object_store: gcs
        schema: v11
        store: boltdb-shipper
storage_config:
    boltdb_shipper:
        active_index_directory: /data/index
        cache_location: /data/boltdb-cache
        shared_store: gcs
    gcs:
        bucket_name: loki
```

Set retention_enabled to true. Without this, the Compactor will only compact tables.

Define schema_config and storage_config to access the storage.

The index period must be 24h.

working_directory is the directory where marked chunks and temporary tables will be saved.

compaction_interval dictates how often compaction and/or retention is applied. If the Compactor falls behind, compaction and/or retention occur as soon as possible.

retention_delete_delay is the delay after which the compactor will delete marked chunks.

retention_delete_worker_count specifies the maximum quantity of goroutine workers instantiated to delete chunks.

## Configuring the retention period

Retention period is configured within the limits_config configuration section.

There are two ways of setting retention policies:

- retention_period which is applied globally.
- retention_stream which is only applied to chunks matching the selector

<blockquote>
<p>The minimum retention period is 24h. </p></blockquote>

This example configures global retention:

```
...
limits_config:
  retention_period: 744h
  retention_stream:
  - selector: '{namespace="dev"}'
    priority: 1
    period: 24h
  per_tenant_override_config: /etc/overrides.yaml
...

```

Per tenant retention can be defined using the /etc/overrides.yaml files. For example:
```
overrides:
    "29":
        retention_period: 168h
        retention_stream:
        - selector: '{namespace="prod"}'
          priority: 2
          period: 336h
        - selector: '{container="loki"}'
          priority: 1
          period: 72h
    "30":
        retention_stream:
        - selector: '{container="nginx"}'
          priority: 1
          period: 24h
```

A rule to apply is selected by choosing the first in this list that matches:

1. If a per-tenant retention_stream matches the current stream, the highest priority is picked.
2. If a global retention_stream matches the current stream, the highest priority is picked.
3. If a per-tenant retention_period is specified, it will be applied.
4. The global retention_period will be selected if nothing else matched.
5. If no global retention_period is specified, the default value of 744h (30days) retention is used.

Stream matching uses the same syntax as Prometheus label matching:

-  =: Select labels that are exactly equal to the provided string.
- !=: Select labels that are not equal to the provided string.
- =~: Select labels that regex-match the provided string.
- !~: Select labels that do not regex-match the provided string.

The example configurations will set these rules:

- All tenants except 29 and 30 in the dev namespace will have a retention period of 24h hours.
- All tenants except 29 and 30 that are not in the dev namespace will have the retention period of 744h.
- For tenant 29:
    - All streams except those in the container loki or in the namespace prod will have retention period of 168h (1 week).
    - All streams in the prod namespace will have a retention period of 336h (2 weeks), even if the container label is loki, since the priority of the prod rule is higher.
    - Streams that have the container label loki but are not in the namespace prod will have a 72h retention period.
- For tenant 30:
    - All streams except those having the container label nginx will have the global retention period of 744h, since there is no override specified.
    - Streams that have the label nginx will have a retention period of 24h.

