# InfluxDB2 + Grafana + Glances stack
## Clonar el repositori:
```
git clone https://github.com/nicolargo/docker-influxdb2-grafana-glances.git
cd docker-influxdb2-grafana-glances
docker pull grafana/grafana
docker pull influxdb
docker pull glances
```

## Posar en marcha:
```
sudo mkdir -p /srv/docker/influxdb2-grafana/grafana/data
sudo mkdir -p /srv/docker/influxdb2-grafana/influxdb2/data
docker-compose up -d
sudo chown -R 1000:1000 /srv/docker/influxdb2-grafana/grafana/data
```

## Mostrar els logs:
```
docker-compose logs
```

## Parar y eliminar:
``` 
docker-compose stop
docker-compose rm
```

## Actualitzar:
```
git pull
docker pull grafana/grafana
docker pull influxdb
docker pull glances
```